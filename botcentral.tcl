# Bot Central
#
# Users must be botnet-master (+t flag)
# on masters bots to use commands
namespace eval BC {

    # Global configuration
    # Masters are the main bots, allowed to send any command
    # to any bot in botnet
    variable masters {"Artus" "Excalibur" "Raspdrop"}
    # statmasters are bots allowed to send commands
    # to statbots (+0 bots)
    variable statmasters {"_BS00"}
    # Log level : error, warn, debug
    variable loglevel "warn"
    
    # Script info datas
    variable author "CrazyCat <https://www.eggdrop.fr>"
    variable version "0.1"
    variable flag ".:BotCentral:."
    
    # (de)crypt key, must be exactly 8 chars long
    variable skey "This KeY is pr1v4t3"

    package require blowfish


    # Master commands
    # .botadd handle address port[/uport] [target[,target]]
    proc dcc.botadd {handle idx text} {
        if {![::BC::isMaster]} { return }
        if {[string trim $text] eq ""} { putdcc [*dcc:help $handle $idx botadd] }
        lassign [split $text] bhandle baddr bports dest
        if {[::BC::isKnownBot $bhandle]} { putdcc $idx "You can't add this bot, $bhandle exists !"; return }
        if {$bports eq ""} { putdcc $idx "The bot port is mandatory"; return }
        lassign [split $bports "/"] bport uport
        ::BC::sendToBots [::BC::makeTargets $dest] ">botadd" "$bhandle $baddr $bport $uport"
        putdcc $idx "Sending botadd $bhandle $baddr $bport to [join $targets "#"]"
    }
    
    # .bodel handle [target[,target]]
    proc dcc.botdel {handle idx text} {
        if {![::BC::isMaster]} { return }
        if {[string trim $text] eq ""} { putdcc [*dcc:help $handle $idx botadd] }
        lassign [split $text] bhandle dest
        if {![::BC::isKnownBot $bhandle]} { putdcc $idx "I don't know $bhandle, sorry"; return }
        ::BC::sendToBots [::BC::makeTargets $dest] ">botdel" "$bhandle"
        putdcc $idx "Sending >botdel $bhandle to [join $targest "#"]"
    }
    
    # Clients procedures
    proc bot.botadd {from key text} {
        if {![::BC::isMaster $from]} { ::BC::sendToBots $from ">boterr" "You are not allowed to do that"; return }
        set text [::BC::decrypt $text]
        lassign [split $text] bhandle baddr bport uport
        if {[::BC::isKnownBot $bhandle]} {
            ::BC::sendToBots $from ">boterr" "I know $bhandle, cannot add him"
            return
        }
        if {[addbot $bhandle $baddr $bport $uport]==0} {
            ::BC::sendToBots $from ">boterr" "Something went wrong when adding $bhandle"
        } else {
            putlog "Done: addbot $bhandle $baddr $bport $uport"
            ::BC::sendToBots $from ">botack" "$::BC::flag successfuly added $bhandle"
        }
    }
    
    proc bot.botdel {from key text} {
        if {![::BC::isMaster $from]} { ::BC::sendToBots $from ">boterr You are not allowed to do that"; return }
        set text [::BC::decrypt $text]
        set bhandle [string trim $text]
        if {![::BC::isKnownBot $bhandle]} {
            ::BC::sendToBots $from ">boterr" "$bhandle is not a known bot"
            return
        }
        if {![matchattr ||+b $bhandle]} {
            ::BC::sendToBots $from ">boterr" "$bhandle seems to be a normal user"
            return
        }
        if {[deluser $bhandle]==0} {
            ::BC::sendToBots $from ">boterr" "Something went wrong when deleting $bhandle"
        } else {
            ::BC::sendToBots $from ">botack" "$::BC::flag successfuly deleted $bhandle"
        }
    }
    
    # internal tools
    proc isMaster {{handle ""}} {
        if {$handle eq ""} { set handle ${::botnet-nick} }
        if {[lsearch -nocase $::BC::masters $handle]<0} { return 0 } else { return 1 }
    }
    
    proc isStatMaster {{handle ""}} {
        if {$handle eq ""} { set handle ${::botnet-nick} }
        if {[lsearch -nocase $::BC::statmasters $handle]<0} { return 0 } else { return 1 }
    }
    
    proc isKnownBot {handle} {
        if {[isbotnick $handle]||[string tolower $handle] eq [string tolower ${::botnet-nick}]} { return 0 }
        set ikb [validuser $handle]
        incr ikb [lsearch -nocase [bots] $handle]
        putlog $ikb
        if {$ikb >= 0} { return 1 } else { return 0 }
    }
    
    proc makeTargets {{dest ""}} {
        if {$dest eq ""} {
            set targets "all"
        } else {
            set targets [split $dest ","]
        }
        return $targets
    }
    
    proc sendToBots {dest cmd {text ""}} {
        set text [::BC::encrypt $text]
        if {$dest eq "all"} {
            putallbots "$cmd $text"
        } else {
            foreach b [split $dest] {
                putbot $b "$cmd $text"
            }
        }
    }
    
    
    proc help {{command ""}} {
        
    }
    
    proc error {from key text} {
        putlog "$::BC::flag Error from $from :$text"
    }
    proc acknowledge {from key text} {
        putlog "$::BC::flag Success from $from :$text"
    }
    
    # 
    proc logger {severity text} {
      if {$::BC::loglevel eq "debug"} {
      
      }
    }
    
    proc init {} {
        loadhelp botcentral.help
        bind bot - >boterr ::BC::error
        bind bot - >botack ::BC::acknowledge
        if {[::BC::isMaster]} {
            # Loading commands only for Masters
            bind dcc +t botadd ::BC::dcc.botadd
            bind dcc +t botdel ::BC::dcc.botdel
            bind dcc +t useradd ::BC::dcc.useradd
            bind dcc +t userdel ::BC::dcc.userdel
        }
        if {[::BC::isMaster] || [::BC::isStatMaster]} {
            # Loading commands for Masters and StatMasters
            bind dcc +t botjoin ::BC::dcc.botjoin
            bind dcc +t botpart ::BC::dcc.botpart
            bind dcc +t botlog ::BC::dcc.botlog
        
        }
        bind bot - >botadd ::BC::bot.botadd
        bind bot - >botdel ::BC::bot.botdel
        putlog "BotCentral $::BC::version by $::BC::author loaded"
    }
    
   # encryption utilities
   proc encrypt {text} {
      return [::blowfish::blowfish -dir encrypt -key $::BC::skey $text]
   }
   proc decrypt {text} {
      return [::blowfish::blowfish -dir decrypt -key $::BC::skey $text]
   }
    
    ::BC::init
}